Rails.application.routes.draw do
  get 'books/:id' => 'books#show'

  get 'books' => 'books#index'
  post 'books' => 'books#create'

  get 'castor' => 'pages#home'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
